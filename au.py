#!/usr/bin/env python3

from argparse import ArgumentParser
from sys import stdout


class Baum(object):
    NEWLN = '\n\r'
    EMPTY = ''
    YEHAH = ', juchee'
    INTRO = '''
Drunten in der grünen Au, steht ein Birnbaum schön blau{yeah}.
    '''.strip()
    ITEMS = [dict(
        answr=None,
        lower='in der',
        quest=None,
        thing='Au',
        upper='Baum',
    ), dict(
        answr='Ein wunderschöner',
        lower='am',
        quest='Was ist an dem',
        thing='Baum',
        upper='Ast',
    ), dict(
        answr='Ein wunderschönes',
        lower='am',
        quest='Was ist an dem',
        thing='Ast',
        upper='Ästchen',
    ), dict(
        answr='Ein wunderschönes',
        lower='am',
        quest='Was ist an dem',
        thing='Ästchen',
        upper='Zweigchen',
    ), dict(
        answr='Ein wunderschönes',
        lower='am',
        quest='Was ist an dem',
        thing='Zweigchen',
        upper='Blatt',
    ), dict(
        answr='Ein wunderschönes',
        lower='am',
        quest='Was ist an dem',
        thing='Blatt',
        upper='Nest',
    ), dict(
        answr='Ein wunderschönes',
        lower='im',
        quest='Was ist in dem',
        thing='Nest',
        upper='Ei',
    ), dict(
        answr='Ein wunderschöner',
        lower='im',
        quest='Was ist in dem',
        thing='Ei',
        upper='Vogel',
    ), dict(
        answr='Eine wunderschönes',
        lower='am',
        quest='Was ist an dem',
        thing='Vogel',
        upper='Feder',
    ), dict(
        answr='Ein wunderschönes',
        lower='aus',
        quest='Was wird mit der',
        thing='Feder',
        upper='Bettchen',
    ), dict(
        answr='Ein wunderschönes',
        lower='im',
        quest='Wer liegt in dem',
        thing='Bettchen',
        upper='Mädchen',
    ), dict(
        answr='Ein wunderschöner',
        lower='beim',
        quest='Wer liegt bei dem',
        thing='Mädchen',
        upper='Bube',
    ), dict(
        answr='Ein wunderschönes',
        lower='von',
        quest='Was bekommen die',
        thing='beiden',
        upper='Kind',
    ), dict(
        answr='Das pflanzt einen wunderschönen',
        lower='vom',
        quest='Was macht dann das',
        thing='Kind',
        upper='Birnbaum',
    ), dict(
        answr='Der steht in der',
        lower='mit dem',
        quest='Wo steht dann der',
        thing='Birnbaum',
        upper='Au',
    )]

    @staticmethod
    def arguments(name):
        parser = ArgumentParser(
            name, add_help=True, epilog='-.-'
        )
        parser.add_argument(
            '-y', '--yeah', action='store_true', default=False
        )

        return parser.parse_args()

    def __init__(self):
        self.args = self.arguments(self.__class__.__name__)

    @property
    def intro(self):
        yield self.INTRO.format(yeah=self.YEHAH if self.args.yeah else '')
        yield self.INTRO.format(yeah=self.EMPTY)

    @property
    def empty(self):
        yield self.EMPTY

    @staticmethod
    def uroll(item):
        yield '''{upper} {lower} {thing}.'''.format(**item)

    @staticmethod
    def liner(item):
        yield '''{quest} {thing}? {answr} {upper}.'''.format(**item)

    @property
    def sing(self):
        yield from self.intro
        for num in range(1, len(self.ITEMS)):
            yield from self.empty
            yield from self.liner(self.ITEMS[num])
            for pos in range(num, -1, -1):
                yield from self.uroll(self.ITEMS[pos])
            yield from self.intro
        yield from self.empty

    @property
    def song(self):
        return self.NEWLN.join(self.sing)

    def __call__(self):
        stdout.write(self.song)
        return True


if __name__ == '__main__':
    exit(not Baum()())
